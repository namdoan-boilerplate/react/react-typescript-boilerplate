import React from 'react';
import Header from '../partials/Header';

type Props = {
  children: React.ReactNode;
};

const HeaderOnly = ({ children }: Props) => {
  return (
    <>
      <Header />
      {children}
    </>
  );
};

export default HeaderOnly;
