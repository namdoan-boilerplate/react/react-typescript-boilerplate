import React from 'react';
import Header from '../partials/Header';
import Sidebar from '../partials/Sidebar';

type Props = {
  children: React.ReactNode;
};

const DefaultLayout = ({ children }: Props) => {
  return (
    <>
      <Header />
      <Sidebar />
      {children}
    </>
  );
};

export default DefaultLayout;
