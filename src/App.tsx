import React, { Fragment } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { DefaultLayout } from './components/Layouts';
import routes from './routes';

function App() {
  return (
    <Router>
      <Routes>
        {routes.map((route, index) => {
          const Element = route.element;
          const Layout = route.layout ? route.layout : route.layout === null ? Fragment : DefaultLayout;

          return (
            <Route
              key={index}
              path={route.path}
              element={
                <Layout>
                  <Element />
                </Layout>
              }
            ></Route>
          );
        })}
      </Routes>
    </Router>
  );
}

export default App;
