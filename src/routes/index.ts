import React from 'react';
import { HeaderOnly } from '~/components/Layouts';
import Home from '~/pages/Home';
import Profile from '~/pages/Profile';
import Search from '~/pages/Search';

type Route = {
  path: string;
  element: React.FC;
  layout?: React.FC<{ children: React.ReactNode }> | null;
};

const routes: Route[] = [
  { path: '/', element: Home },
  { path: '/profile', element: Profile, layout: HeaderOnly },
  { path: '/search', element: Search, layout: null },
];

export default routes;
